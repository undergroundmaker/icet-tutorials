import glob
import pandas as pd
import numpy as np
from ase.units import kB
from mchammer import DataContainer
import matplotlib.pyplot as plt

import seaborn as sns
sns.set_context('talk')


# parameters
size = 12
eq_steps = 5000


# collect data
fnames = glob.glob('canonical_runs/dc_size{}_*.dc'.format(size))

records = []
for fname in fnames:

    # ensemble parameters
    dc = DataContainer.read(fname)
    n_atoms = dc.ensemble_parameters['n_atoms']
    n_Si = dc.ensemble_parameters['n_atoms_Si']
    T = dc.ensemble_parameters['temperature']
    c_Si = n_Si / n_atoms

    # thermodynamic data
    energies = dc.get_data('potential', start=eq_steps)
    sros = dc.get_data('sro_Ge_1', start=eq_steps)
    acceptance_ratio = dc.get_data('acceptance_ratio', start=eq_steps)

    heat_capacity = np.var(energies) / (kB*T**2) / n_atoms
    energies /= n_atoms

    row = dict(temperature=T, c_Si=c_Si, energy=energies.mean(), energy_std=energies.std(),
               sro=sros.mean(), sro_std=sros.std(), heat_capacity=heat_capacity,
               acceptance_ratio=acceptance_ratio.mean())
    records.append(row)

df_all = pd.DataFrame(records)


# plotting
fig = plt.figure(figsize=(15, 9))
ax1 = fig.add_subplot(2, 3, 1)
ax2 = fig.add_subplot(2, 3, 2)
ax3 = fig.add_subplot(2, 3, 3)
ax4 = fig.add_subplot(2, 3, 4)
ax5 = fig.add_subplot(2, 3, 5)
ax6 = fig.add_subplot(2, 3, 6)

for c_Si in sorted(df_all.c_Si.unique()):
    df = df_all[df_all.c_Si == c_Si]
    df = df.sort_values(by='temperature')
    label = '$c_{Si}$' + '={:.3f}'.format(c_Si)
    ax1.plot(df.temperature, df.sro, label=label)
    ax2.plot(df.temperature, 1e3*df.energy, label=label)
    ax3.plot(df.temperature, 1e6*df.heat_capacity, label=label)
    ax4.plot(df.temperature, df.sro_std, label=label)
    ax5.plot(df.temperature, 1e3*df.energy_std, label=label)
    ax6.plot(df.temperature, 100 * df.acceptance_ratio, label=label)

ax1.set_ylabel('SRO-parameter')
ax2.set_ylabel('Energy (meV/atom)')
ax3.set_ylabel('Heat capacity ($\mu$eV/atom/K)')
ax4.set_ylabel('std SRO')
ax5.set_ylabel('std Energy (meV/atom)')
ax6.set_ylabel('Acceptance Ratio (%)')


for ax in [ax1, ax2, ax3, ax4, ax5, ax6]:
    ax.set_xlim(df_all.temperature.min(), df_all.temperature.max())
    ax.legend(fontsize=12)
    ax.set_xlabel('Temperature (K)')

fig.tight_layout()
fig.savefig('pdf/canonical_mc.pdf')
plt.show()
